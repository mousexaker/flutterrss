import 'package:flutter/material.dart';
import 'package:flutter_rss/src/blocs/rss_feed_details_bloc.dart';
import 'package:webview_flutter/webview_flutter.dart';

class FeedDetails extends StatefulWidget {
  final RssFeedDetailsBloc bloc;
  final String _url;
  final String _title;

  FeedDetails(this.bloc, this._url, this._title);

  @override
  State<StatefulWidget> createState() => FeedDetailsState(_url, _title);
}

class FeedDetailsState extends State<FeedDetails> {
  final url;
  final title;

  FeedDetailsState(this.url, this.title);

  @override
  void initState() {
    widget.bloc.toString();
    super.initState();
  }

  @override
  void dispose() {
    widget.bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title)),
      body: WebView(initialUrl: url),
    );
  }
}
