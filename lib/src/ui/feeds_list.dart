import 'package:flutter/material.dart';
import 'package:flutter_rss/src/blocs/rss_feeds_bloc.dart';
import 'package:flutter_rss/src/models/feed_model.dart';
import 'package:flutter_rss/src/models/item_model.dart';

class FeedsList extends StatefulWidget {
  final RssFeedsBloc _bloc;

  FeedsList(this._bloc);

  @override
  State<StatefulWidget> createState() => FeedsListState();
}

class FeedsListState extends State<FeedsList> {
  @override
  void initState() {
    super.initState();
    widget._bloc.fetchAllFeeds();
  }

  @override
  void dispose() {
    widget._bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Rss")),
        body: StreamBuilder(
            stream: widget._bloc.allFeeds,
            builder: (context, AsyncSnapshot<FeedModel> snapshot) {
              if (snapshot.hasData) {
                return buildList(snapshot);
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              return Center(child: CircularProgressIndicator());
            }));
  }

  Widget buildList(AsyncSnapshot<FeedModel> snapshot) {
    return ListView.separated(
        itemCount: snapshot.data.items.length,
        separatorBuilder: (context, index) =>
            Divider(
              height: 1.0,
              color: Colors.grey[400],
            ),
        itemBuilder: (BuildContext context, int index) {
          var item = snapshot.data.items[index];
          return buildListItem(item);
        });
  }

  Widget buildListItem(ItemModel item) {
    return ListTile(
      title: Text(item.title, style: TextStyle(fontWeight: FontWeight.bold)),
      subtitle: Text(item.description,
          maxLines: 2, style: TextStyle(color: Colors.grey[500])),
      enabled: true,
      contentPadding: const EdgeInsets.all(32.0),
      onTap: () => _openDetails(item),
    );
  }

  void _openDetails(ItemModel item) {
    Navigator.pushNamed(context, 'movieDetail',
        arguments: item);
  }
}
