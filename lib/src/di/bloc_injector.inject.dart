import 'bloc_injector.dart' as _i1;
import 'bloc_module.dart' as _i2;
import 'package:http/src/client.dart' as _i3;
import '../resources/rss_provider.dart' as _i4;
import '../resources/repository.dart' as _i5;
import 'dart:async' as _i6;
import '../app.dart' as _i7;
import '../blocs/rss_feeds_bloc.dart' as _i8;
import '../blocs/rss_feed_details_bloc.dart' as _i9;

class BlocInjector$Injector implements _i1.BlocInjector {
  BlocInjector$Injector._(this._blocModule);

  final _i2.BlocModule _blocModule;

  _i3.Client _singletonClient;

  _i4.RssProvider _singletonRssProvider;

  _i5.Repository _singletonRepository;

  static _i6.Future<_i1.BlocInjector> create(_i2.BlocModule blocModule) async {
    final injector = BlocInjector$Injector._(blocModule);

    return injector;
  }

  _i7.App _createApp() =>
      _i7.App(_createRssFeedsBloc(), _createRssFeedDetailsBloc());
  _i8.RssFeedsBloc _createRssFeedsBloc() =>
      _i8.RssFeedsBloc(_createRepository());
  _i5.Repository _createRepository() =>
      _singletonRepository ??= _blocModule.repository(_createRssProvider());
  _i4.RssProvider _createRssProvider() =>
      _singletonRssProvider ??= _blocModule.rssProvider(_createClient());
  _i3.Client _createClient() => _singletonClient ??= _blocModule.client();
  _i9.RssFeedDetailsBloc _createRssFeedDetailsBloc() =>
      _i9.RssFeedDetailsBloc(_createRepository());
  @override
  _i7.App get app => _createApp();
}
