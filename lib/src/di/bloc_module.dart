import 'package:flutter_rss/src/blocs/bloc_base.dart';
import 'package:flutter_rss/src/blocs/rss_feed_details_bloc.dart';
import 'package:flutter_rss/src/blocs/rss_feeds_bloc.dart';
import 'package:flutter_rss/src/resources/repository.dart';
import 'package:flutter_rss/src/resources/rss_provider.dart';
import 'package:http/http.dart';
import 'package:inject/inject.dart';

@module
class BlocModule {
  @provide
  @singleton
  Client client() => Client();

  @provide
  @singleton
  RssProvider rssProvider(Client client) => RssProvider(client);

  @provide
  @singleton
  Repository repository(RssProvider provider) => Repository(provider);

  @provide
  BlocBase rssFeedBloc(Repository repository) => RssFeedsBloc(repository);

  @provide
  BlocBase rssFeedDetailsBloc(Repository repository) =>
      RssFeedDetailsBloc(repository);
}
