import 'package:flutter_rss/src/blocs/bloc_base.dart';
import 'package:flutter_rss/src/models/feed_model.dart';
import 'package:flutter_rss/src/resources/repository.dart';
import 'package:inject/inject.dart';
import 'package:rxdart/rxdart.dart';

class RssFeedsBloc extends BlocBase {
  static const url = "https://fakty.ua/rss_feed/ukraina";
  final Repository _repository;
  final _rssFetcher = PublishSubject<FeedModel>();

  @provide
  RssFeedsBloc(this._repository);

  Observable<FeedModel> get allFeeds => _rssFetcher.stream;

  fetchAllFeeds() async {
    var feeds = await _repository.fetchFeed(url);
    _rssFetcher.sink.add(feeds);
  }

  dispose() {
    _rssFetcher.close();
  }
}
