import 'package:flutter_rss/src/blocs/bloc_base.dart';
import 'package:flutter_rss/src/resources/repository.dart';
import 'package:inject/inject.dart';

class RssFeedDetailsBloc extends BlocBase {
  final Repository _repository;

  @provide
  RssFeedDetailsBloc(this._repository);

  @override
  void dispose() {}
}
