import 'package:flutter_rss/src/models/feed_model.dart';
import 'package:flutter_rss/src/resources/rss_provider.dart';
import 'package:inject/inject.dart';

class Repository {
  final RssProvider _rssProvider;

  @provide
  Repository(this._rssProvider);

  Future<FeedModel> fetchFeed(String url) => _rssProvider.fetchFeedsList(url);
}
