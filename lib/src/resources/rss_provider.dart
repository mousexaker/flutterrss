import 'package:flutter_rss/src/models/feed_model.dart';
import 'package:http/http.dart';
import 'package:inject/inject.dart';
import 'package:xml/xml.dart';

class RssProvider {
  final Client _client;

  @provide
  RssProvider(this._client);

  Future<FeedModel> fetchFeedsList(String url) async {
    var response = await _client.get(url);
    if (response.statusCode == 200) {
      return FeedModel.fromXml(parse(response.body));
    } else {
      throw Exception("Fail to load feed");
    }
  }
}

class Status {}
