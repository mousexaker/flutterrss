import 'package:flutter/material.dart';
import 'package:flutter_rss/src/blocs/rss_feed_details_bloc.dart';
import 'package:flutter_rss/src/blocs/rss_feeds_bloc.dart';
import 'package:flutter_rss/src/models/item_model.dart';
import 'package:flutter_rss/src/ui/feed_details.dart';
import 'package:flutter_rss/src/ui/feeds_list.dart';
import 'package:inject/inject.dart';

class App extends StatelessWidget {
  final RssFeedsBloc rssFeedsBloc;
  final RssFeedDetailsBloc rssFeedDetailsBloc;

  @provide
  App(this.rssFeedsBloc, this.rssFeedDetailsBloc);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light(),
      initialRoute: '/',
      onGenerateRoute: (settings) {
        if (settings.name == 'movieDetail') {
          final ItemModel result = settings.arguments;
          return MaterialPageRoute(builder: (context) {
            return FeedDetails(
              rssFeedDetailsBloc,
              result.link,
              result.title,
            );
          });
        } else
          return null;
      },
      routes: {
        '/': (context) => FeedsList(rssFeedsBloc),
      },
    );
  }
}
