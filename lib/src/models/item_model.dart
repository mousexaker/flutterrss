import 'package:xml/xml.dart';

class ItemModel {
  String _title;
  String _link;
  String _description;
  String _category;
  String _guid;
  String _pubDate;

  ItemModel.fromXml(XmlElement element) {
    _title = element.findElements("title").first.text;
    _link = element.findElements("link").first.text;
    _description = element.findElements("description").first.text;
    _category = element.findElements("category").first.text;
    _guid = element.findElements("guid").first.text;
    _pubDate = element.findElements("pubDate").first.text;
  }

  String get title => _title;

  String get link => _link;

  String get description => _description;

  String get category => _category;

  String get guid => _guid;

  String get pubDate => _pubDate;
}
