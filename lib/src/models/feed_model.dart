import 'package:flutter_rss/src/models/item_model.dart';
import 'package:xml/xml.dart';

class FeedModel {
  List<ItemModel> _items;

  FeedModel.fromXml(XmlDocument doc) {
    _items = [];
    for (var i in doc.findAllElements("item")) {
      var item = ItemModel.fromXml(i);
      _items.add(item);
    }
  }

  List<ItemModel> get items => _items;
}
